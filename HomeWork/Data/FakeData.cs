﻿using System;
using System.Collections.Generic;
using System.Text;
using HomeWork.Models;

namespace HomeWork.Data
{
    public class FakeData
    {
        public static FamilyMember GetFakeData()
        {
            var first = new FamilyMember("Иван Андреевич", Gender.Man, 1738, 0, null);

            List<FamilyMember> secondLevel = new List<FamilyMember>();
            var child1 = new FamilyMember("Петр Иванович", Gender.Man, 1756, 1, first);
            

            List<FamilyMember> thirdLevel1 = new List<FamilyMember>();
            thirdLevel1.Add(new FamilyMember("Глафира Петровна", Gender.Women, 1777, 2, child1));
            thirdLevel1.Add(new FamilyMember("Анна Петровна", Gender.Women, 1780, 2, child1));
            child1.AddRangeChild(thirdLevel1);


            var child2 = new FamilyMember("Дарья Ивановна", Gender.Women, 1758, 1, first);
            List<FamilyMember> thirdLevel2 = new List<FamilyMember>();
            thirdLevel2.Add(new FamilyMember("Иван Николаевич", Gender.Man, 1805, 2, child2));
            child2.AddRangeChild(thirdLevel2);

            secondLevel.Add(child1);
            
            secondLevel.Add(child2);

            first.AddRangeChild(secondLevel);

            return first;
        }
    }
}
