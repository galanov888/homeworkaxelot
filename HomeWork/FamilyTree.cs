﻿using System;
using System.Collections.Generic;
using System.Text;
using HomeWork.Models;
using HomeWork.Data;
using System.Linq;

namespace HomeWork
{
    public class FamilyTree : IDisposable
    {
        public FamilyTree()
        {
            fileReader = new FamilyFileReader();
        }
        private FamilyMember familyMember;

        FamilyFileReader fileReader;

        private bool CheckCanFindByLevel(int familyMemberLevel, int enteredMemberLevel)
        {
            return (familyMemberLevel - enteredMemberLevel) >= 0;
        }


        #region FindBlock
        private IEnumerable<FamilyMember> GetAllFamilyByName(string name, IEnumerable<FamilyMember> familyMembers)
        {
            List<FamilyMember> list = new List<FamilyMember>();

            foreach (var familyMember in familyMembers)
            {
                if (familyMember.GetChildrenCount() > 0)
                {
                    list.AddRange(GetAllFamilyByName(name, familyMember.GetChildren()));
                }
                var testName = familyMember.GetName();
                if (name.CompareTo(testName) == 0)
                {
                    list.Add(familyMember);
                }
            }
            return list;
        }
        #endregion

        private IEnumerable<FamilyMember> FindFamilyMembers(IEnumerable<FamilyMember> people, int levelKindred, Gender gender)
        {
            var result = new List<FamilyMember>();
            // список найденных людей
            foreach (var familyMember in people)
            {
                if (!CheckCanFindByLevel(familyMember.GetLevel(), levelKindred))
                {
                    continue;
                }
                string notCheckedName;
                var upperParent = GetUpperLevel(familyMember, levelKindred, out notCheckedName);
                var checkedChildren = upperParent.GetChildren().Where(ch => ch.GetName() != notCheckedName);
                foreach (var child in checkedChildren)
                {
                    result.AddRange(GetFamily(child, familyMember.GetLevel(), gender));
                }
            }
            return result;
        }

        private IEnumerable<FamilyMember> GetFamily(FamilyMember familyMember, int checkedLevel, Gender gender)
        {
            var findedMembers = new List<FamilyMember>();
            if (familyMember.GetLevel() < checkedLevel)
            {
                if (familyMember.HasChildren())
                {
                    foreach (var child in familyMember.GetChildren())
                    {
                        findedMembers.AddRange(GetFamily(child, checkedLevel, gender));
                    }
                }
            }
            else
            {
                if (familyMember.GetGender() == gender)
                {
                    findedMembers.Add(familyMember);
                }
            }
            return findedMembers; ;
        }

        private FamilyMember GetUpperLevel(FamilyMember familyMember, int countLevels, out string notCheckedName)
        {
            notCheckedName = "";
            FamilyMember returnedFamilyMember = familyMember;
            for (int i = 0; i < countLevels; i++)
            {
                notCheckedName = returnedFamilyMember.GetName();
                returnedFamilyMember = returnedFamilyMember.GetParent();
            }
            return returnedFamilyMember;
        }

        public string GetKindred(string name, Gender gender, int levelKindred)
        {
            //найти объекты по имени
            var list = new List<FamilyMember>();
            if (familyMember == null)
            {
                familyMember = fileReader.GetFileData();
            }
            list.Add(this.familyMember);
            var familyWithName = GetAllFamilyByName(name, list);
            if (!familyWithName.Any())
            {
                Console.WriteLine($"Не найдено членов семьи с именем {name}");
                return null;
            }
            //получить родственников
            var result = FindFamilyMembers(familyWithName, levelKindred, gender);

            return GetNames(result);
        }

        private readonly string MessageNotFound = "Родственники не найдены";

        private string GetNames(IEnumerable<FamilyMember> familyMembers)
        {
            if (!familyMembers.Any())
            {
                return MessageNotFound;
            }
            return String.Join(";", familyMembers.Select(fm => fm.GetName()));

        }

        public void Dispose()
        {
            fileReader.Dispose();
        }
    }


}
