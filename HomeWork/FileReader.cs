﻿using System;
using System.Collections.Generic;
using System.Text;
using HomeWork.Models;
using System.IO;

namespace HomeWork
{
    public class FamilyFileReader: IDisposable
    {
        private readonly string fileDirectory = "Data";
        private readonly string fileName = "family.txt";

        private string GetFilePath()
        {
            var filePath = $"{Directory.GetCurrentDirectory()}\\{fileDirectory}\\{fileName}";
            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException($"Файл {fileName} не найден.");
            }
            return filePath;
        }

        public FamilyMember GetFileData()
        {
            FamilyMember familyMember = default;
            var path = GetFilePath();
            using (StreamReader streamReader = new StreamReader(path))
            {
                string line;
                while ((line = streamReader.ReadLine()) != null)
                {

                    int level = GetLevel(line);
                    if (level == 0)
                    {
                        familyMember = GetParseData(line, null);
                        continue;
                    }
                    if (level > familyMember.GetLevel())
                    {
                        var newFamilyMember = GetParseData(line, familyMember);
                        familyMember.AddChild(newFamilyMember);
                        familyMember = newFamilyMember;
                        continue;
                    }
                    if (level == familyMember.GetLevel())
                    {
                        familyMember = (FamilyMember)familyMember.GetParent();
                        var newFamilyMember = GetParseData(line, familyMember);
                        familyMember.AddChild(newFamilyMember);
                        familyMember = newFamilyMember;
                        continue;
                    }
                    if (level < familyMember.GetLevel())
                    {
                        while (level <= familyMember.GetLevel())
                        {
                            familyMember = (FamilyMember)familyMember.GetParent();
                        }
                        var newFamilyMember = GetParseData(line, familyMember);
                        familyMember.AddChild(newFamilyMember);
                        familyMember = newFamilyMember;
                    }
                }
            }
            while (familyMember.GetLevel() != 0)
            {
                familyMember = familyMember.GetParent();
            }
            return familyMember;
        }

        private int GetLevel(string line)
        {
            int countTab = default;
            foreach (var element in line)
            {
                if (element.Equals('\t'))
                {
                    countTab++;
                }
                else
                {
                    break;
                }
            }
            return countTab;
        }
        
        private FamilyMember GetParseData(string line, FamilyMember? parent)
        {
            int countTab = GetLevel(line);
            
            line = line.TrimStart('\t'); 
            var lineData = line.Split(new char[] { '|' });
            var name = lineData[0].Trim();
            var gender = GetGender(lineData[1]);
            var year = GetBirthYear(lineData[2]);
            return new FamilyMember(name, gender, year, countTab, parent);
        }

        private int GetBirthYear(string year)
        {
            year = year.Trim();
            if (!int.TryParse(year, out int birthYear))
            {
                throw new ArgumentException("Не удалось преобразовать год рождения");
            }
            return birthYear;
        }

        private Gender GetGender(string gender)
        {
            gender = gender.Trim();
            if (gender.Length > 1)
            {
                throw new ArgumentException("Длинна поля Пол превышено");
            }

            if (gender.Length == 0)
            {
                throw new ArgumentException("Длинна поля равно 0");
            }

            switch (gender.ToUpper())
            {
                case ("М"):
                    return Gender.Man;
                case ("Ж"):
                    return Gender.Women;
                default:
                    throw new ArgumentException("Введено не верное значение");
            }
        }

        public void Dispose()
        {
            
        }
    }
}
