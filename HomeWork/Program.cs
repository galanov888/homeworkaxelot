﻿using System;
using HomeWork.Data;
using HomeWork.Models;
using System.Linq;
using System.Collections.Generic;

namespace HomeWork
{
    class Program
    {
        static void Main(string[] args)
        {
            
            try
            {
                Console.WriteLine("Введите имя члена семьи");
                var name = Console.ReadLine().Trim();
                Console.WriteLine("Введите тип родства (Б - брат или С - сестра)");
                var gender = Converter.GetGender( Console.ReadLine().Trim());

                Console.WriteLine("Введите степень родства – целое число, где 1 – родной брат или сестра, 2- двоюродный и тд");
                var levelKindred = Converter.GetLevelKindred( Console.ReadLine().Trim());
                using (var familyTree = new FamilyTree())
                {
                    Console.WriteLine(familyTree.GetKindred(name, gender, levelKindred));
                } 
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Ошибка: {ex.Message}");
            
            }
        }
    }
}
