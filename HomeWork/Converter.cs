﻿using System;
using System.Collections.Generic;
using System.Text;
using HomeWork.Models;

namespace HomeWork
{
    public static class Converter
    {
        public static Gender GetGender(string gender)
        {
            gender = gender.Trim();
            if (gender.Length > 1)
            {
                throw new ArgumentException("Длинна поля Пол превышено");
            }

            if (gender.Length == 0)
            {
                throw new ArgumentException("Длинна поля равно 0");
            }

            switch (gender.ToUpper())
            {
                case ("Б"):
                    return Gender.Man;
                case ("С"):
                    return Gender.Women;
                default:
                    throw new ArgumentException("Введено не верное значение");
            }
        }

        public static int GetLevelKindred(string value)
        {
            int levelKindred;
            if (!Int32.TryParse(value, out levelKindred))
            {
                throw new ArgumentException("Не верное значение. Невозможно преобразовать в число.");
            }
            if (levelKindred < 1 && levelKindred > 20)
            {
                throw new ArgumentException("Не верное значение. Значение должно быть от 1 до 20");
            }
            return levelKindred;
        }
    }
}
