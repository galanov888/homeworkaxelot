﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace HomeWork.Models
{

    public class FamilyMember
    {
        public FamilyMember(string name, Gender gender, int birthDay, int levelAffinity, FamilyMember parent)
        {
            this.name = name;
            this.gender = gender;
            this.birthDay = birthDay;
            this.children = new List<FamilyMember>();
            this.levelAffinity = levelAffinity;
            this.parent = parent;
            
        }

        private FamilyMember parent;

        private string name;

        private Gender gender;

        private int birthDay;

        private int levelAffinity;

        List<FamilyMember> children;

        public FamilyMember GetParent()
        {
            return parent;
        }

        public string GetName()
        {
            return name;
        }

        public Gender GetGender()
        {
            return gender;
        }

        public int GetBirthDay()
        {
            return birthDay;
        }

        public int GetLevel()
        {
            return this.levelAffinity;
        }

       
        public void AddChild(FamilyMember child)
        {
            children.Add(child);
        }
        public void AddRangeChild(IEnumerable<FamilyMember> child)
        {
            children.AddRange(child);
        }

        public FamilyMember GetChildByName(string name)
        {
            return children.FirstOrDefault(ch => ch.name == name);
        }

        public IEnumerable<FamilyMember> GetChildren()
        {
            return children;
        }

        public int GetChildrenCount()
        {
            return children.Count;
        }

        public bool HasChildren()
        {
            return children.Any();
        }
    }

    public enum Gender
    {
        Man = 0,
        Women = 1
    }
}


